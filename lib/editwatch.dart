import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EditWatch extends StatefulWidget {
  EditWatch({Key? key , this.map}) : super(key: key);

  Map? map;
  @override
  State<EditWatch> createState() => _EditWatchState();
}

class _EditWatchState extends State<EditWatch> {

  var formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController imageController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {

    nameController.text = widget.map == null?'':widget.map!['name'];
    imageController.text = widget.map == null?'':widget.map!["avatar"];
    priceController.text = widget.map == null?'':widget.map!['price'];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add User"),
      ),
      body: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                controller: nameController,
                decoration: InputDecoration(
                  hintText: "Enter Name",
                ),
                validator: (value) {
                  if(value == null || value.isEmpty){
                    return "Enter The Name";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: priceController,
                decoration: InputDecoration(
                  hintText: "Enter Price",
                ),
                validator: (value) {
                  if(value == null || value.isEmpty){
                    return "Enter The Id";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: imageController,
                decoration: InputDecoration(
                  hintText: "Enter image",
                ),
                validator: (value) {
                  if(value == null || value.isEmpty){
                    return "Enter The Image";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    isLoading = true;
                  });
                  if(formKey.currentState!.validate()){
                    if(widget.map == null){
                      insertUser().then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                    else{
                      updateUser(widget.map!['id']).then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                  }
                },
                child: Container(
                  width: 180,
                  height: 50,
                  padding: EdgeInsets.only(top: 10 , right: 50 , bottom: 10 , left: 50),
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: isLoading?CircularProgressIndicator():Text("Submit",style: TextStyle(color: Colors.white),),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> insertUser() async {

    Map map = {};
    map["name"] = nameController.text.toString();
    map["id"] = priceController.text.toString();
    map["avatar"] = imageController.text.toString();

    http.Response res = await http.post(Uri.parse('https://630ee802498924524a815ff2.mockapi.io/student'),body: map);
  }

  Future<void> updateUser(id) async {

    Map map = {};
    map["name"] = nameController.text.toString();
    map["price"] = priceController.text.toString();
    map["avatar"] = imageController.text.toString();

    http.Response  res = await http.put(Uri.parse('https://630ee802498924524a815ff2.mockapi.io/student/$id'),body: map);
  }
}
