import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rolexscreen/editwatch.dart';
import 'package:rolexscreen/thirdpage.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final Shader linearGradient = LinearGradient(
    colors: [
      Color.fromRGBO(219, 199, 169, 1),
      Color.fromRGBO(170, 146, 120, 1),
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  void initState() {
    getWatchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: FutureBuilder<http.Response>(
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  dynamic jsonData = jsonDecode(snapshot.data!.body.toString());
                  return Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            // color: Colors.tealAccent,
                            height: 70,
                            width: 70,
                            //color: Colors.tealAccent,
                            padding: EdgeInsets.only(
                              top: 40,
                            ),
                            // child: Icon(Icons., color: Colors.white),
                            child: Image.asset('assets/images/Symbol1_rm_bg.png',
                                fit: BoxFit.contain),
                          ),
                          Container(
                            //color: Colors.amber,
                            height: 50,
                            width: 50,
                            margin: EdgeInsets.only(top: 40),
                            child: Image.asset(
                                'assets/images/topcentersymbol (2).png',
                                fit: BoxFit.contain),
                          ),
                          Container(
                            height: 72,
                            width: 70,
                            padding: EdgeInsets.only(
                              top: 42,
                            ),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                  return EditWatch(map: null,);
                                },),).then((value) {
                                  setState(() {

                                  });
                                },);
                              },
                              child: Icon(
                                Icons.add,
                                color: Color.fromRGBO(219, 199, 169, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Divider(
                        color: Color.fromRGBO(219, 199, 169, 1),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        child: Text(
                          'ROLEX WATCHES',
                          style: TextStyle(
                            fontFamily: 'Lora',
                            //fontWeight: FontWeight.w500,
                            fontSize: 22,
                            foreground: Paint()..shader = linearGradient,
                            height: 1.5,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Divider(
                        color: Color.fromRGBO(219, 199, 169, 1),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: Text(
                              'The Collection',
                              style: TextStyle(
                                // fontFamily: 'Lora',
                                fontSize: 18,
                                color: Colors.white,
                                // foreground: Paint()..shader = linearGradient,
                                height: 1.5,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Text(
                              'See all',
                              style: TextStyle(
                                //fontFamily: 'Lora',
                                fontSize: 14,
                                foreground: Paint()..shader = linearGradient,
                                height: 1.5,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 380,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: jsonData.length,
                          itemBuilder: (context, index) {
                            return ContainerMaker(
                              jsonData[index]["avatar"],
                              jsonData[index]["name"],
                              jsonData[index]["price"],
                              jsonData[index],
                              jsonData[index]["id"],
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 160),
                        child: Text(
                          'New Watches 2022',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        width: 340,
                        height: 150,
                        decoration: BoxDecoration(
                          color: Colors.white10,
                          borderRadius: BorderRadius.circular(3),
                        ),
                        child: Row(
                          children: [
                            Container(
                              width: 90,
                              //color: Colors.amber,
                              child: Image.asset(
                                'assets/images/watch3_rm_bg.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'YATCH MASTER  42',
                                  style: TextStyle(
                                    fontFamily: 'Lora',
                                    fontSize: 20,
                                    foreground: Paint()..shader = linearGradient,
                                    height: 2.8,
                                  ),
                                ),
                                Text(
                                  'Glowing with new brilliance',
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.white,
                                    height: 1.5,
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.currency_pound_outlined,
                                      color: Color.fromRGBO(170, 146, 120, 1),
                                    ),
                                    Text(
                                      '101,999',
                                      style: TextStyle(
                                        fontFamily: 'Lora',
                                        fontSize: 23,
                                        foreground: Paint()
                                          ..shader = linearGradient,
                                        height: 1.5,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
              future: getWatchData(),
            ),
          ),
        ),
      ),
    );
  }

  Future<http.Response> getWatchData() async {
    http.Response res = await http
        .get(Uri.parse('https://630ee802498924524a815ff2.mockapi.io/student'));
    print(res.body);
    return res;
  }

  Future<void> deleteUser(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://630ee802498924524a815ff2.mockapi.io/student/$id'));
  }

  Widget ContainerMaker(image, name, price, jsonData , id) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return Thirdpage(image: image , name: name, price: price, map: jsonData,);
        },),).then((value) {
          setState(() {

          });
        },);
      },
      child: Container(
        height: 380,
        margin: EdgeInsets.only(left: 15, right: 10),
        width: 230,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color.fromRGBO(240, 255, 240, 0.3),
              Color.fromRGBO(200, 200, 200, 0.2),
              Color.fromRGBO(100, 100, 100, 0.2),
            ],
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Stack(
          children: [
            Column(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Image.network(
                    // height: 250,
                    // width: 100,
                    image,
                    fit: BoxFit.cover,
                  ),
                  margin: EdgeInsets.only(top: 20),
                  height: 260,
                  width: 180,
                  //color: Colors.amber,
                ),
                Text(
                  name,
                  style: TextStyle(
                    fontFamily: 'Lora',
                    //fontWeight: FontWeight.w500,
                    fontSize: 20,
                    foreground: Paint()..shader = linearGradient,
                    height: 1.5,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'The Cosmopolitan Watch',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.currency_pound_outlined,
                        color: Color.fromRGBO(219, 199, 169, 1), size: 20),
                    Text(
                      price,
                      style: TextStyle(
                        fontFamily: 'Lora',
                        //fontWeight: FontWeight.w500,
                        fontSize: 20,
                        foreground: Paint()..shader = linearGradient,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              top: 10,
              right: 10,
              child: InkWell(
                onTap: () {
                  deleteUser(id).then((value) {
                    setState(() {

                    });
                  },);
                },
                child: Icon(
                  Icons.delete,
                  color: Color.fromRGBO(219, 199, 169, 1),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
